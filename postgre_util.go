package swpostgre

import (
	"database/sql"
	"fmt"
)

func rowsToMap(rows *sql.Rows) (RowsMap, error) {
	defer rows.Close()

	cols, err := rows.Columns()
	if err != nil {
		return make(RowsMap, 0), err
	}

	colsLen := len(cols)
	colsPtr := make([]interface{}, colsLen)
	colsVal := make([]interface{}, colsLen)

	for idx := range colsVal {
		colsPtr[idx] = &colsVal[idx]
	}

	mpRows := make(RowsMap, 0)
	for rows.Next() {
		mpRow := make(map[string]string)
		rows.Scan(colsPtr...)
		for idx, col := range colsVal {
			if col == nil {
				continue
			}

			mpRow[cols[idx]] = fmt.Sprintf("%v", col)
		}

		mpRows = append(mpRows, mpRow)
	}

	return mpRows, nil
}
