package swpostgre

import (
	"database/sql"
	"fmt"

	_ "github.com/lib/pq"
)

func New(opts DBOpts) (*PostgreDB, error) {

	dsn := fmt.Sprintf("postgres://%s:%s@%s:%d/%s?sslmode=disable", opts.User, opts.Password, opts.Host, opts.Port, opts.Database)
	db, err := sql.Open("postgres", dsn)
	if err != nil {
		return nil, err
	}

	if opts.MaxOpenConns == 0 {
		opts.MaxOpenConns = 2
	}

	db.SetMaxIdleConns(opts.MaxIdleConns)
	db.SetMaxOpenConns(opts.MaxOpenConns)

	err = db.Ping()
	if err != nil {
		return nil, err
	}

	return &PostgreDB{
		conn: db,
		dsn:  dsn,
	}, nil
}
